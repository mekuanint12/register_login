package main

import (
	"context"
	"reflect"

	"github.com/cucumber/godog"
	_ "github.com/lib/pq"
	db "github.com/mekuanint12/simple_api/db/sqlc"
)

func userLogin(User *godog.Table) (db.Account, error) {

	user := User.Rows[1]
	UserName := user.Cells[0].Value
	Password := user.Cells[1].Value
	data := db.GetAccountParams{UserName, Password}

	message, err := testQueries.GetAccount(context.Background(), data)
	return message, err
}

func errorMessageDisplayedWithTheWithInvalidData(loginInfo *godog.Table) error {
	var err error
	_, e := userLogin(loginInfo)
	if e != nil {
		err = nil
	}
	return err
}

func userSendsAGETRequiest() error {
	return nil
}

func userShouldSeeTheDetails(loginInfo *godog.Table) error {
	var err error
	message, _ := userLogin(loginInfo)
	if reflect.TypeOf(message).String() == "Account" {
		err = nil
	}
	return err
}
