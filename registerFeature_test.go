package main

import (
	"context"
	"reflect"

	"github.com/cucumber/godog"
	_ "github.com/lib/pq"
	db "github.com/mekuanint12/simple_api/db/sqlc"
)

var testQueries *db.Queries

func userDetails(User *godog.Table) (db.Account, error) {
	// var data AccountParams
	user := User.Rows[1]
	UserName := user.Cells[0].Value
	FirstName := user.Cells[1].Value
	LastName := user.Cells[2].Value
	Password := user.Cells[3].Value
	Email := user.Cells[4].Value
	data := db.CreateAccountParams{UserName, FirstName, LastName, Password, Email}

	message, err := testQueries.CreateAccount(context.Background(), data)
	return message, err
}

func errorMessageDisplayedWithInvalidData() error {
	return godog.ErrPending
}

func userEntersTheFollowingDetails(arg1 *godog.Table) error {

	_, err := userDetails(arg1)
	return err
}

func userEntersWrongCharacters(wrongInfo *godog.Table) error {
	var err error
	_, e := userDetails(wrongInfo)
	if e != nil {
		err = nil
	}
	return err
}

func userIsOnRegisterPage(arg1 string) error {
	return nil
}

func userSendsPOSTRequest(arg1 string) error {
	return nil
}

func userShouldSeeAccountDetails(arg1 *godog.Table) error {
	var err error
	message, _ := userDetails(arg1)
	if reflect.TypeOf(message).String() == "Account" {
		err = nil
	}
	return err
}

func InitializeScenario(ctx *godog.ScenarioContext) {
	ctx.Step(`^Error message displayed with invalid data$`, errorMessageDisplayedWithInvalidData)
	ctx.Step(`^User enters the following details$`, userEntersTheFollowingDetails)
	ctx.Step(`^User enters wrong characters$`, userEntersWrongCharacters)
	ctx.Step(`^User is on "([^"]*)" page$`, userIsOnRegisterPage)
	ctx.Step(`^User sends "([^"]*)" POST request$`, userSendsPOSTRequest)
	ctx.Step(`^User should see "([^"]*)" Details$`, userShouldSeeAccountDetails)
	ctx.Step(`^Error message displayed with the with invalid data$`, errorMessageDisplayedWithTheWithInvalidData)
	ctx.Step(`^User sends a GET requiest$`, userSendsAGETRequiest)
	ctx.Step(`^User should see the "([^"]*)" Details$`, userShouldSeeTheDetails)
}
