@alluserflow
Feature: User Registration

  Background: 
    Given User is on "/register" page

  @registration
  Scenario: Creating New User Account
    When User enters the following details
    | <UserName> | <FirstName> | <LastName> | <Password>  |   <Email>       |
    |   Meku1    |   Meku      |    Legese  |   123456    | meku@meku.com   |
    And User sends "register" POST request
    Then User should see "My Account" Details

  @registration
  Scenario: User Does NOT Follow the form validations
    When User enters wrong characters
    | <UserName> | <FirstName> | <LastName> | <Password>  |   <Email>       |
    |   Meku1    |   Meku      |    Legese  |   123456    | @meku.com   |
   Then Error message displayed with invalid data