@alluserflow
Feature: User Login

  Background: 
    Given User is on "/login" page

  @login
  Scenario: Login Functionality
    When User enters the following details
    | <UserName> | <Password>  |   
    |   Meku1    |   123456#   | 
    And User sends a GET requiest
    Then User should see the "My Account" Details

  @login
  Scenario: Unsuccessful login
    When User enters the following details
    | <UserName> | <Password>  |   
    |   Meku1    |   123456#   | 
    And User sends a GET requiest
    Then Error message displayed with the with invalid data
